var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.locals.views = [
  {page: 0, view: 1},
  {page: 1, view: 1},
  {page: 2, view: 1},
  {page: 3, view: 1}
];

app.locals.events = [
  {
    id: 1,
    name: "Football match",
    dateStart: '2016-12-10T20:19:00.000Z',
    dateEnd: '2016-12-10T20:21:00.000Z',
    location: 'Baltimor',
    owner: {
      id: 2,
      firstName: 'John',
      lastName: 'Brudniy'
    },
    users: [
      {
        id: 3,
        firstName: 'Alex',
        lastName: 'Levinski'
      },
      {
        id: 4,
        firstName: 'Tori',
        lastName: 'Lee'
      }
    ]
  },
  {
    id: 5,
    name: "Football super match",
    dateStart: '2016-12-10T20:19:00.000Z',
    dateEnd: '2016-12-11T20:21:00.000Z',
    location: 'LA',
    owner: {
      id: 5,
      firstName: 'John',
      lastName: 'Brudniy'
    },
    users: [
      {
        id: 7,
        firstName: 'Alex',
        lastName: 'Levinski'
      },
      {
        id: 8,
        firstName: 'Tori',
        lastName: 'Lee'
      }
    ]
  },

  {
    id: 9,
    name: "Baseball match",
    dateStart: '2016-12-12T20:19:00.000Z',
    dateEnd: '2016-12-12T20:21:00.000Z',
    location: 'San Paolo',
    owner: {
      id: 10,
      firstName: 'John',
      lastName: 'Brudniy'
    },
    users: [
      {
        id: 11,
        firstName: 'Alex',
        lastName: 'Levinski'
      },
      {
        id: 12,
        firstName: 'Jane',
        lastName: 'Osera'
      }
    ]
  }
];


var SSE = require('express-sse');
var sse = new SSE(["array", "containing", "initial", "content", "(optional)"]);

app.get('/stream', sse.init);

sse.send('message 1');
sse.send('message 2', 'myEvent');



app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
